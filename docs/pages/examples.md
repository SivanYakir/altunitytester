# Examples

We used AltUnity Tester to test some sample games to help you understand better how to use it. 
We plan to add more examples in the near future.

```eval_rst
.. note::
    Example test projects below can be run on any platform.
```

**1.** Example test projects created for different languages and platforms:

* [Standalone Build | C# tests](https://gitlab.com/altom/altunity/examples/alttrashcat-tests-csharp)
* [Android | Python tests](https://gitlab.com/altom/altunity/examples/alttrashcat-tests-python)
* [iOS | Java tests](https://gitlab.com/altom/altunity/examples/alttrashcat-tests---java)

    You can get the sample game from the [Unity Asset Store - link](https://assetstore.unity.com/packages/essentials/tutorial-projects/endless-runner-sample-game-87901).

**2.** Example test project for multiplayer features:

* [Multiplayer - iOS / Android | Python tests](https://gitlab.com/altom/altunity/examples/alttanksmultiplayer-test-python)

    You can get the sample game from the [Unity Asset Store - link](https://assetstore.unity.com/packages/essentials/tutorial-projects/tanks-reference-project-80165).